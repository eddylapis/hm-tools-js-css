package top.hmtools.jsCss.jsManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import top.hmtools.jsCss.autoConfiguration.JsCssAutoConfiguration;
import top.hmtools.jsCss.common.ResourcesBean;

/**
 * javascript相关操作的controller
 * @author HyboJ
 * 创建日期：2017-9-26下午10:33:07
 */
@Controller
//@ConditionalOnBean(value=IsEnableJsCss.class)
public class JavascriptController {
	
	private final Logger logger = LoggerFactory.getLogger(JavascriptController.class);
	
	@Autowired
	private IJavascriptManager javascriptManager;
	
	@Autowired
	private JsCssAutoConfiguration jsCssAutoConfiguration;
	
	/**
	 * 获取合并的javascript文件内容
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="#{'${"+JsCssAutoConfiguration.js_uri+":}'?:'/js'}/{jsNames}",method={RequestMethod.GET,RequestMethod.POST})
	public void getJS(@PathVariable("jsNames")String jsNames,HttpServletRequest request,HttpServletResponse response){
		this.getJS(jsNames, "UTF-8", request, response);
	}

	/**
	 * 获取合并的javascript文件内容
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="#{'${"+JsCssAutoConfiguration.js_uri+":}'?:'/js'}/encoding/{encoding}/{jsNames}",method={RequestMethod.GET,RequestMethod.POST})
	public void getJS(@PathVariable("jsNames")String jsNames,@PathVariable("encoding")String encoding,HttpServletRequest request,HttpServletResponse response){
		PrintWriter writer = null;
		try {
			String jsContent = this.javascriptManager.getJs(jsNames);
			response.reset();
			response.setBufferSize(2048);
			response.setCharacterEncoding(encoding);
			response.setContentLength(jsContent.getBytes(encoding).length);
			response.setContentType("application/javascript");
			response.setStatus(HttpServletResponse.SC_OK);
			writer = response.getWriter();
			writer.print(jsContent);
			writer.flush();
		} catch (Exception e) {
			this.logger.error("获取js文件失败："+e.getMessage(),e);
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,e.getMessage());
			} catch (IOException e1) {
				this.logger.error(e1.getMessage(),e1);
			}
		}finally{
			if(writer != null){
				writer.close();
			}
		}
	}
	
	/**
	 * 刷新javascript缓存库
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="#{'${"+JsCssAutoConfiguration.refresh_js_uri+":}'?:'/refresh/js'}",method={RequestMethod.GET,RequestMethod.POST})
	public void refreshJS(HttpServletRequest request,HttpServletResponse response){
		PrintWriter writer = null;
		try {
			boolean refresh = this.javascriptManager.refresh();
			
			response.reset();
			response.setBufferSize(2048);
			response.setCharacterEncoding(this.jsCssAutoConfiguration.getEncoding());
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			writer = response.getWriter();
			writer.println("refresh javascript repertory status is "+refresh);
			writer.println("<br>3秒后自动跳转到JS列表页面……");
			writer.printf("<br><a href=\"%1$s\">%2$s</a>",this.jsCssAutoConfiguration.getListJsUri(),"手动跳转  "+this.jsCssAutoConfiguration.getListJsUri());
			response.setHeader("refresh","3;URL="+this.jsCssAutoConfiguration.getListJsUri()+"");
			writer.flush();
		} catch (Exception e) {
			this.logger.error("刷新js缓存失败："+e.getMessage(),e);
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,e.getMessage());
			} catch (IOException e1) {
				this.logger.error(e1.getMessage(),e1);
			}
		}finally{
			if(writer != null){
				writer.close();
			}
		}
	}
	
	/**
	 * 获取所有javascript缓存库中js文件名
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="#{'${"+JsCssAutoConfiguration.list_js_uri+":}'?:'/list/js'}",method={RequestMethod.GET,RequestMethod.POST})
	public void listJsMenu(HttpServletRequest request,HttpServletResponse response){
		PrintWriter writer = null;
		try {
			List<ResourcesBean> listResourcesBeans = this.javascriptManager.listResourcesBeans();
			
			response.reset();
			response.setBufferSize(2048);
			response.setCharacterEncoding(this.jsCssAutoConfiguration.getEncoding());
//			response.setContentLength(jsContent.length());
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			writer = response.getWriter();
			writer.println("<title>JavaScript文件列表（总计："+listResourcesBeans.size()+"条）</title>");
			writer.println("<style type=\"text/css\">a:link,a:visited{text-decoration:none;}tr:nth-child(2n){background-color: #E0FFFF;}tr:nth-child(2n+1){background-color:#FFFFFF;}</style>");
			writer.println("<h4>All javascript file name is (counts:"+listResourcesBeans.size()+"):</h4>");
			writer.println("<a href=\""+this.jsCssAutoConfiguration.getRefreshJsUri()+"\"><h4>Refresh js store</h4></a>");
			writer.println("<table>");
			for(ResourcesBean jsTmp:listResourcesBeans){
				writer.println("<tr>");
				writer.printf("<td><a href=\"%1$s\">%2$s</a></td>",this.jsCssAutoConfiguration.getJsUri()+"/"+jsTmp.getKeyName(),jsTmp.getFileFullName());
				writer.printf("<td>&nbsp;&nbsp;<a href=\"%1$s\">%2$s</a></td>",this.jsCssAutoConfiguration.getJsUri()+"/"+jsTmp.getKeyName(),jsTmp.getFileRelativePath());
				writer.printf("<td>&nbsp;&nbsp;<a href=\"%1$s\">%2$s</a></td>",this.jsCssAutoConfiguration.getJsUri()+"/encoding/UTF-8/"+jsTmp.getKeyName(),"UTF-8");
				writer.printf("<td>&nbsp;&nbsp;<a href=\"%1$s\">%2$s</a></td>",this.jsCssAutoConfiguration.getJsUri()+"/encoding/GBK/"+jsTmp.getKeyName(),"GBK");
				writer.println("</tr>");
				writer.flush();
			}
			writer.println("</table>");
		} catch (Exception e) {
			this.logger.error("获取js列表失败："+e.getMessage(),e);
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,e.getMessage());
			} catch (IOException e1) {
				this.logger.error(e1.getMessage(),e1);
			}
		}finally{
			if(writer != null){
				writer.close();
			}
		}
	}
}
