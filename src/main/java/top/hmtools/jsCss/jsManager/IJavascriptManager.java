package top.hmtools.jsCss.jsManager;

import java.util.List;

import top.hmtools.jsCss.common.ResourcesBean;

/**
 * javascript管理者接口
 * @author HyboJ
 * 创建日期：2017-9-26下午9:08:08
 */
public interface IJavascriptManager {

	/**
	 * 初始化
	 */
	void init();
	
	/**
	 * 销毁
	 */
	void destory();
	
	/**
	 * 根据javascript文件名集合获取javascript文件的字符串内容
	 * @param jsNames
	 * @return
	 */
	String getJs(String jsNames);

	/**
	 * 根据javascript文件名集合获取javascript文件的字符串内容
	 * @param jsNames
	 * @return
	 */
	String getJs(List<String> jsNames);

	/**
	 * 根据javascript文件名集合获取javascript文件的字符串内容
	 * @param jsNames
	 * @return
	 */
	String getJs(String[] jsNames);
	
	/**
	 * 刷新缓存
	 * @return
	 */
	boolean refresh();
	
	/**
	 * 获取所有js缓存库中的js文件名称
	 * @return
	 */
	List<String> listJsFilenames();
	
	/**
	 * 获取所有静态资源信息（javascript文件信息集合）
	 * @return
	 */
	List<ResourcesBean> listResourcesBeans();
}
