package top.hmtools.jsCss.cssManager;

import java.util.List;

import top.hmtools.jsCss.common.ResourcesBean;

/**
 * css相关操作管理者
 * @author HyboJ
 * 创建日期：2017-9-27下午4:30:44
 */
public interface ICssManager {

	/**
	 * 初始化
	 */
	void init();
	
	/**
	 * 销毁
	 */
	void destory();
	
	/**
	 * 刷新缓存
	 * @return
	 */
	boolean refresh();
	
	/**
	 * 获取合并的css文件内容
	 * @param cssNames
	 * @return
	 */
	String getCss(String cssNames);
	
	/**
	 * 获取合并的css文件内容
	 * @param cssNames
	 * @return
	 */
	String getCss(List<String> cssNames);
	
	/**
	 * 获取合并的css文件内容
	 * @param cssNames
	 * @return
	 */
	String getCss(String[] cssNames);
	
	/**
	 * 获取所有css缓存库中的css文件名称
	 * @return
	 */
	List<String> listCssFilenames();
	
	/**
	 * 获取所有静态资源信息（css文件信息集合）
	 * @return
	 */
	List<ResourcesBean> listResourcesBeans();
}
