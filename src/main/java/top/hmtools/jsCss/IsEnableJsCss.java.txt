package top.hmtools.jsCss;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * 是否启用本组件的开关bean，作为同一条件供其它bean使用
 */
@Component
@ConditionalOnProperty(prefix = "hm_tools.js_css", value = "enabled", matchIfMissing = true) // 当配置文件中有hm_tools.js_css.enabled=false时，则不创建本类中的bean，如果为hm_tools.js_css.enabled=true，或者没有该条配置（注解默认为true），则创建bean
public class IsEnableJsCss {

}
