[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/spring-boot-starter-js-css.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22spring-boot-starter-js-css%22)  [![996.icu](https://img.shields.io/badge/link-996.icu-red.svg)](https://996.icu)

# hm-tools-js-css 功能
- 根据URL请求参数将多个javascript文件合并成一个javascript文件返回给请求者。
- 根据URL请求参数将多个css文件合并成一个css文件返回给请求者。
- 支持从当前运行的spring boot工程jar包中读取javascript，css文件。
- 支持在请求URL携带字符编码名称获取相应的字符编码格式的js、css文件。
- 支持替换css文件中url引用资源的相对路径为可访问的绝对路径。

# 使用场景
一个html页面中必然会需要引用JavaScript文件及css文件，以JavaScript文件为例，比如要分别引用 a.js,b.js。一般情况下，会使用2个“<script>”标签引用，即：
```
<script src="a.js"></script>
<script src="b.js"></script>
```
这样会存在浏览器向服务器发送2次http请求，才能获取完所需要的JavaScript文件。
使用本工具后，则只需要一个标签就可以引入完2个JavaScript文件，即：
```
<script src="a.js,b.js"></script>
```

# 使用说明
本工具包是适配spring boot工程开发，在以spring boot框架为基础的项目工程中的pom.xml文件引入：
```
<dependency>
    <groupId>top.hmtools</groupId>
    <artifactId>spring-boot-starter-js-css</artifactId>
    <version>0.2.3</version>
</dependency>
```
即可使用，若需要更改配置，请参照一下配置说明。

# 缺省的获取javascript文件URL示例：
- http://{网址}:{端口号}/js/encoding/{字符编码名称}/aaa.js,bbb.js
- 或者：http://{网址}:{端口号}/js/aaa.js,bbb.js
- {字符编码名称}可选参数，不填时，缺省UTF-8，可以使用GBK,UTF-8，GB2312，UTF-16等Java支持的合法字符编码名称，否则会报错。
- 缺省的查询所有JavaScript文件列表页面（URI可通过配置文件修改）：http://{网址}:{端口号}/list/js

# 缺省的获取css文件URL示例：
- http://{网址}:{端口号}/css/encoding/{字符编码名称}/aaa.css,bbb.css
- 或者：http://{网址}:{端口号}/css/aaa.css,bbb.css
- {字符编码名称}可选参数，不填时，缺省UTF-8，可以使用GBK,UTF-8，GB2312，UTF-16等Java支持的合法字符编码名称，否则会报错。
- 缺省的查询所有css文件列表页面（URI可通过配置文件修改）：http://{网址}:{端口号}/list/css

# 配置说明：
- hm_tools.js_css.enabled=true	是否启用本jar包组件功能，当配置为“true”时或者不配置该值时均表示启用，为“false”时则不启用。
- hm_tools.js_css.js_uri=/get_js		配置获取javascript文件内容的请求uri
- hm_tools.js_css.css_uri=/get_css	配置获取CSS文件内容的请求uri
- hm_tools.js_css.refresh_js_uri=/refresh_js	配置刷新javascript文件缓存内容的请求uri
- hm_tools.js_css.refresh_css_uri=/refresh_css	配置刷新css文件缓存内容的请求uri
- hm_tools.js_css.js_files_paths=static		配置获取javascript文件内容的磁盘路径集合（均相对于工程项目的classpath），以英文逗号（,）分隔
- hm_tools.js_css.css_files_paths=static		配置获取css文件内容的磁盘路径集合（均相对于工程项目的classpath），以英文逗号（,）分隔
- hm_tools.js_css.encoding=UTF-8		获取的文件内容的字符编码格式名称，缺省为“UTF-8”。
- hm_tools.js_css.list_js_uri=/list_js		获取所有javascript文件列表请求uri
- hm_tools.js_css.list_css_uri=/list_css		获取所有css文件列表请求uri
- hm_tools.js_css.src_uri=src	通用的获取静态资源文件内容uri，仅获取单个文件，且该文件必须在配置的js_files_paths、css_files_paths路径下，用于解决css文件中含有相对路径引用其它文件的问题。

#### 如何使用本组件的快照版本

1. 在自己的私服建立一个快照代理仓库即可。https://oss.sonatype.org/content/repositories/snapshots/
![如何使用快照版本](/images/howToUseSnapshot.jpg)

2. 将建好的参考纳入公共库
![如何使用快照版本](/images/howToUseSnapshot-2.jpg)